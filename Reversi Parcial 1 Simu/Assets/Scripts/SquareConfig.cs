﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquareConfig : MonoBehaviour
{
    public Transform wToken;
    public Transform bToken;

    // instanciar sonda
    public Transform probeDOWN;
    public Transform probeUP;
    public Transform probeRight;
    public Transform probeLeft;
    public Transform probeUL;
    public Transform probeUR;
    public Transform probeDL;
    public Transform probeDR;


    private void OnMouseDown() // funciona igual que el OnTriggerEnter, solo que esta sujeto al click del mouse y donde clickea
    {
        ChangeTokenTurn();
    }

    private void OnMouseEnter()
    {
        GetComponent<SpriteRenderer>().color = new Color(0.5f, 0.5f, 0.5f);
    }

    private void OnMouseExit()
    {
        GetComponent<SpriteRenderer>().color = new Color(1, 1, 1);
    }

    void ChangeTokenTurn()
    {
        if (GM.currentTurnToken == "w") //creo una variable estatica para verla desde cualquier parte del proyecto, esto lo hago para verificar de quien es el turno
        {
            Instantiate(wToken, transform.position, wToken.rotation); // instancia un token en la casilla clickeada
            /* GM.currentTurnToken = "b";*/ // (estaba comentado) si la variable es "w" es el turno de with, al hacer click cambio a "b" para saber que ahora toca a black
            StartCoroutine(WaitToChangeColorTokenB());
            GetComponent<BoxCollider2D>().enabled = false; //accedo al componente box collider 2D y lo desactivo para que el jugador no haga trampa, sobre escribiendo el sprite

            // instanciar sondas
            Instantiate(probeDOWN, transform.position, probeDOWN.rotation); // instanciamos la sonda que va a verificar los token
            Instantiate(probeUP, transform.position, probeUP.rotation);
            Instantiate(probeRight, transform.position, probeRight.rotation);
            Instantiate(probeLeft, transform.position, probeLeft.rotation);
            Instantiate(probeUL, transform.position, probeUL.rotation);
            Instantiate(probeUR, transform.position, probeUR.rotation);
            Instantiate(probeDL, transform.position, probeDL.rotation);
            Instantiate(probeDR, transform.position, probeDR.rotation);

            // puntaje
            GM.pointsWhite += 1;
        }
        else
        if (GM.currentTurnToken == "b")
        {
            Instantiate(bToken, transform.position, bToken.rotation);
            /* GM.currentTurnToken = "w";*/ // estaba comentado
            StartCoroutine(WaitToChangeColorTokenW()); // lo hice con una corrutina para que de tiempo ah que todas las fichas se cambien
            GetComponent<BoxCollider2D>().enabled = false;

            // instanciar sondas
            Instantiate(probeDOWN, transform.position, probeDOWN.rotation); // instanciamos la sonda que va a verificar los token
            Instantiate(probeUP, transform.position, probeUP.rotation);
            Instantiate(probeRight, transform.position, probeRight.rotation);
            Instantiate(probeLeft, transform.position, probeLeft.rotation);
            Instantiate(probeUL, transform.position, probeUL.rotation);
            Instantiate(probeUR, transform.position, probeUR.rotation);
            Instantiate(probeDL, transform.position, probeDL.rotation);
            Instantiate(probeDR, transform.position, probeDR.rotation);

            // puntaje
            GM.pointsBlack += 1;
        }
    }

    IEnumerator WaitToChangeColorTokenB()
    {
        yield return new WaitForSeconds(0.5f);
        GM.currentTurnToken = "b";
        GM.ulChangePosition = "n";
    }

    IEnumerator WaitToChangeColorTokenW()
    {
        yield return new WaitForSeconds(0.5f);
        GM.currentTurnToken = "w";
        GM.ulChangePosition = "n";
    }
}
