﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GM : MonoBehaviour
{
    public Transform square;

    public static string currentTurnToken="w"; // instancio una variable (public static) para poder acceder desde todo el proyecto
    public static string ulChangePosition = "n";

    public static int pointsWhite = 0;
    public static int pointsBlack = 0;

    public GameObject buttonExit;

    void Start()
    {
        buttonExit.gameObject.SetActive(false);
        InstantiateBoard();
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            buttonExit.gameObject.SetActive(true);
        }
    }

    void InstantiateBoard()
    {
        // solucione el tema de los espacios entre cada bloque, tenia que subir un punto al flotante X que determina el final del bucle for y poner 1f como resultado final
        //(0.75f = 12, 1.2f = 8, 0.95f = 10) tengo que solucionar el problema que me esta dando mucho espacio entre cada instancia 
        for (float y = 4; y > -4; y -= 1f)
        {
            for (float x = -4; x < 4; x += 1f)
            {
                Instantiate(square, new Vector2(x, y), square.rotation);
            }
        }

    }

}
