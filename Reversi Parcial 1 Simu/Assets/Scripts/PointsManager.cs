﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointsManager : MonoBehaviour
{
    public Text wToken;
    public Text bToken;
   
    void Update()
    {
        PointsLogic();
    }

    void PointsLogic()
    {
        wToken.text = "W Tokens: " + GM.pointsWhite.ToString();
        bToken.text = "B Tokens: " + GM.pointsBlack.ToString();
    }
}
