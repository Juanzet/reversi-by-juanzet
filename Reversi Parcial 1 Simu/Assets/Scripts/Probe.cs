﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Probe : MonoBehaviour
{
    private string tagObject;

    void Start()
    {
        tagObject = this.gameObject.name.Substring(0,3);
      
        CheckGameObject();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "e")
        {
            GM.ulChangePosition = "r"; // debe revertirse
            Destroy(gameObject);
        }

        if (other.tag == GM.currentTurnToken)
        {
            GM.ulChangePosition = "y"; // debe cambiar de color
            Destroy(gameObject);
        }

        if (other.tag != GM.currentTurnToken)
        {
            GM.ulChangePosition = "n"; // NO deebe cambiar de color
            other.tag = gameObject.tag;
        }
    }

    void CheckGameObject()
    {
        switch (tagObject)
        {
            case "pDO":
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, -5);
                break;
            case "pUP":
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, 5);
                break;
            case "pRI":
                GetComponent<Rigidbody2D>().velocity = new Vector2(5, 0);
                break;
            case "pLE":
                GetComponent<Rigidbody2D>().velocity = new Vector2(-5, 0);
                break;
            case "pUL":
                GetComponent<Rigidbody2D>().velocity = new Vector2(5, 5);
                break;
            case "pUR":
                GetComponent<Rigidbody2D>().velocity = new Vector2(-5, 5);
                break;
            case "pDL":
                GetComponent<Rigidbody2D>().velocity = new Vector2(5, -5);
                break;
            case "pDR":
                GetComponent<Rigidbody2D>().velocity = new Vector2(-5, -5);
                break;
            default:
                Debug.Log("NO FUNCIONO");
                break;
        }
    }

}
