﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TokenChange : MonoBehaviour
{
    public string currentToken;
 
    void Start()
    {
        currentToken = gameObject.tag;
        GetComponent<CircleCollider2D>().enabled = false;
        StartCoroutine(WaitOn());
    }

    void Update()
    {
        LogicChangeColorToken();
    }

    void LogicChangeColorToken()
    {
        if (gameObject.tag == "UL") // verificamos que la sonda colicione con el token
        {
            //--------- cambiar color ---------
            if ((GM.ulChangePosition == "y") && (currentToken == "b"))
            {
                //cambia de negro a blanco
                GetComponent<SpriteRenderer>().color = new Color(1, 1, 1);
                gameObject.tag = "w"; 
                GM.pointsBlack -= 1; // solo es necesario poner -1 porque al estar ejecutandose en multiples objetos va a restar a todos por igual
                GM.pointsWhite += 1;
            }

            if ((GM.ulChangePosition == "y") && (currentToken == "w"))
            {
                //cambia de blanco a negro
                GetComponent<SpriteRenderer>().color = new Color(0, 0, 0);
                gameObject.tag = "b"; 
                GM.pointsBlack += 1; // repito solo es necesario poneer +1 porque esta ejecutandose en multiples objetos
                GM.pointsWhite -= 1;
            }

            //--------- revertir cambio ---------

            if ((GM.ulChangePosition == "r") && (currentToken == "b"))
            {
                gameObject.tag = "b";
            }

            if ((GM.ulChangePosition == "r") && (currentToken == "w"))
            {
                gameObject.tag = "w";
            }
        }
    }

    IEnumerator WaitOn()
    {
        yield return new WaitForSeconds(1);
        GetComponent<CircleCollider2D>().enabled = true;
    }
}
